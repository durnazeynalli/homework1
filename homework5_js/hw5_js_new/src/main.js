let tabs = document.querySelector(".tabs");
let activeTab = document.querySelectorAll(".active");
let text = document.querySelector(".tabs-content");
let shownText = document.querySelectorAll(".hide");

tabs.addEventListener("click", function (e) {
    let target = e.target;
    if (activeTab) {
        for (let el of tabs.children) {
            el.classList.remove("active");
        }
    }
    target.classList.add("active");
});

tabs.addEventListener("click", function (e) {
    let target = e.target;
    let targetIndex = target.className[11];
    let className = text.children[targetIndex - 1];
    if (shownText) {
        if (shownText.length) {
            for (let el of shownText) {
                el.classList.remove("show");
            }
        }
    }
    className.classList.add("show");
});

function addNumberToTab() {
    let i = 1;
    for (let content of tabs.children) {
        content.classList.add(i);
        i++;
    }
}
addNumberToTab();






