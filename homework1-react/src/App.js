import React, { useState } from 'react';

import Button from "./components/Button";
import {Modal} from "./components/Modal";

function App() {

  const [firstModalStatus, setFirstModalStatusStatus] = useState(false);
  const [secondModalStatus, setSecondModalStatusStatus] = useState(false);

  const toggleFirstModal = () => setFirstModalStatusStatus(v => !v);

  const toggleSecondModal = () => setSecondModalStatusStatus(v => !v);


  return (
      <div className="App">

            <Button
                backgroundColor='green'
                text='Open first modal'
                onClick={toggleFirstModal}
            />

          <Button
              backgroundColor='blue'
              text='Open second modal'
              onClick={toggleSecondModal}
          />

          {firstModalStatus && (
                <Modal
                    header='Do you want to delete this file?'
                    closeIcon={true}
                    text='Once you delete this file, it won’t be possible to undo this action.
                            Are you sure you want to delete it?'
                    close={toggleFirstModal}
                    actions={[
                          <Button
                              key = {1}
                              backgroundColor='#b3382c'
                              text='Ok'
                              onClick={() => alert('first')}
                          />,
                          <Button
                              key = {2}
                              backgroundColor='#b3382c'
                              text='Cancel'
                              onClick={toggleFirstModal}
                          />
                    ]}
                />
          )}

          {secondModalStatus && (
                  <Modal
                      header='Do you want to discard this file?'
                      closeIcon={true}
                      text='If you discard this file, it will deleted.
                            Are you sure you want to discard it?'
                      close={toggleSecondModal}
                      actions={[
                          <Button
                              key = {3}
                              backgroundColor='red'
                              text='Discard'
                              onClick={() => alert('second')}
                          />,
                          <Button
                              key = {4}
                              backgroundColor='green'
                              text='Keep'
                              onClick={toggleSecondModal}
                          />
                      ]}
                  />
                  )}
      </div>
  );
}

export default App;
