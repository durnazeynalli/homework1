function filterBY(a, arg) {

    let array = [];
    arg = 0;

    for (let value of a) {
        if (typeof value != typeof arg || value == null) {
            array.push(value);
        }
    }
    return array;
}

console.log(filterBY(['hello', 'world', 23, null, '23']));
