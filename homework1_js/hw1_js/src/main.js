const name = prompt('Your name: ');
const age = +prompt('Your age: ');

function checkAge(name, age, denied, confirmed, granted) {
    if (age < 18) {
        denied();
    } else if (age >= 18 && age <= 22) {
        confirmed();
    } else {
        granted();
    }
};

function denied() {
    alert('You are not allowed to visit this website.');
};

function confirmed() {
    const question = confirm('Are you sure you want to continue?');
    if (question) {
        alert('Welcome, ' + name);
    } else {
        alert('You are not allowed to visit this website.');
    }
};

function granted() {
    alert('Welcome, ' + name);
};

checkAge(name, age, denied, confirmed, granted);