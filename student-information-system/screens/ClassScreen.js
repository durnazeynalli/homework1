import React from 'react';
import { StyleSheet, View } from 'react-native';
import {
	Footer,
	GradeShower,
	ProgressBar,
	Materials,
	Seperator,
	ClassField,
	Header,
	ScheduleContainer
} from '../components';
import { COLORS } from '../styles/colors';
import { ICONS_LIGHT } from '../styles/iconsLight';
import { Information } from '../components/Information';

export const ClassScreen = () => {
	return (
		<View style={styles.container}>
			<Header title="Mobile dev.1" iconRight={ICONS_LIGHT.classLight} />
			{/* a plachholder title for now */}
			<GradeShower />
			<Information />
			<ProgressBar color={COLORS.componentsDark} height={24} progress={49} />
			<Seperator distance={17} color={COLORS.backgroundDark} />
			<View style={styles.row}>
				<Materials />
				<ClassField
					heading="Homeworks"
					date="due : 14.20.2020"
					topic="Homework 2   14 june     100"
					style={{ width: '48%' }}
				/>
			</View>
			<ScheduleContainer />

			<Footer style={styles.footer} />
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: COLORS.backgroundLight
	},
	footer: {
		position: 'absolute',
		bottom: 0
	},
	row: {
		width: '100%',
		flexDirection: 'row'
	}
});

