import React from 'react';
import {StyleSheet, View, ScrollView, TouchableOpacity} from 'react-native';
import {
    CreateBtn,
    Footer,
    HomeScreenHeader,
    NewsContainer
} from '../components';
import { COLORS } from '../styles/colors';

export const HomeStack = () => {
    return (
        <View style={styles.container}>
            <HomeScreenHeader/>
            <ScrollView>
                <NewsContainer/>

            </ScrollView>

            <CreateBtn/>

            <Footer style={styles.footer} />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.backgroundLight
    },
    footer: {
        position: 'absolute',
        bottom: 0
    },
    row: {
        width: '100%',
        flexDirection: 'row'
    },
});