export const COLORS = Object.freeze({
	backgroundLight: '#EDEDED', //background(header footer) color for light theme, text color for dark theme
	backgroundDark: '#11082C', //background color for dark theme
	componentsLight: '#E5E5E5', //components(comunity messages,homeworks tab, grey part of progress bar) for light thmeme
	componentsDark: '#392A58', // header, footer, compenents(comunity posts, classes,homeworks, dark part of progress bar) for dark theme and text color, drawer backgraund for light theme
	acsentColor: '#CF007C', // acsent color for both dark and light <ThemeProvider theme={}>
	commentsColor: '#483F5C' // comments for dark theme
});
