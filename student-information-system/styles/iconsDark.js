import shareDark from '../assets/icons/dark-theme/share-dark.png';
import starDark from '../assets/icons/dark-theme/star-dark.png';
import homeDark from '../assets/icons/dark-theme/home-dark.png';
import heartDark from '../assets/icons/dark-theme/heart-dark.png';
import feedbackDrawerDark from '../assets/icons/dark-theme/feedback-drawer-dark.png';
import drawerDark from '../assets/icons/dark-theme/drawer-dark.png';
import commentDark from '../assets/icons/dark-theme/comment-dark.png';
import classDark from '../assets/icons/dark-theme/class-dark.png';
import callindarDark from '../assets/icons/dark-theme/callindar-dark.png';
import callindarCheckDark from '../assets/icons/dark-theme/calendar-check-dark.png';
import callindarAddDark from '../assets/icons/dark-theme/calendar-add-dark.png';
import backDark from '../assets/icons/dark-theme/back-dark.png';

export const ICONS_DARK = Object.freeze({
	homeDark, //home button for footer
	callindarDark, //callindar btn for footer
	classDark, //class btn for footer

	shareDark, //share btn for comunity posts
	starDark, //star btn for feedback screen
	heartDark, //like btn for cominity posts and comments
	feedbackDrawerDark, //feedback btn for drawer
	drawerDark, //toggle drawer icon for header,
	commentDark, //comments btn for community posts ,
	callindarCheckDark, //calindar icon for scedual part of class screen
	callindarAddDark, //Add to callindar btn for comunity posts
	backDark //back arrow for header
});
