import user from '../assets/icons/user.png';
import password from '../assets/icons/password.png';
import homeLight from '../assets/icons/light-theme/home-light.png';
import heartLight from '../assets/icons/light-theme/heart-light.png';
import starLight from '../assets/icons/light-theme/star-light.png';
import shareLight from '../assets/icons/light-theme/share-light.png';
import feedbackLight from '../assets/icons/light-theme/feedback-light.png';
import drawerLight from '../assets/icons/light-theme/drawer-light.png';
import commentLight from '../assets/icons/light-theme/comment-light.png';
import classLight from '../assets/icons/light-theme/class-light.png';
import checkCallendarLight from '../assets/icons/light-theme/check-calendar-light.png';
import callendarLight from '../assets/icons/light-theme/calendar-light.png';
import backLight from '../assets/icons/light-theme/back-light.png';
import avatarLight from '../assets/icons/light-theme/avatar-light.png';
import addCalendarLight from '../assets/icons/light-theme/add-calendar-light.png';

export const ICONS_LIGHT = Object.freeze({
	user, //user icon for log in page
	password, //password icon for log in page

	homeLight, //home button for footer
	classLight, //class btn for footer
	callendarLight, // callindar btn for foter
	addCalendarLight,
	heartLight, //like button for comments and comunity posts
	starLight, //star icon for feedback screen
	shareLight, //share btn for comunity posts
	feedbackLight, //feedbck icon for drawer,
	drawerLight, //toggle drawer btn for heder,
	commentLight, //comments btn for comunity posts,
	checkCallendarLight, //calindar icon for scedual part of class screen
	backLight, //back btn for header
	avatarLight //avatar icon for profile pic
});
