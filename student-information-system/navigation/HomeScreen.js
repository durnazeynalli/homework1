import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import { HomeStack } from '../screens';
const { Navigator, Screen } = createStackNavigator();

export const HomeScreen = () => (
    <Navigator headerMode={"none"}>
        <Screen name="Home" component={HomeStack} />
    </Navigator>
)