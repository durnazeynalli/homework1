import React from 'react'
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import { ClassStack } from './ClassStack';
import {HomeScreen} from "./HomeScreen";

const { Screen, Navigator } = createDrawerNavigator();

export const RootDrawer=()=>(
   <NavigationContainer>
      <Navigator>
         <Screen name="ClassStack" component={ClassStack}/>
         <Screen name="HomeScreen" component={ HomeScreen }/>
      </Navigator>
   </NavigationContainer>
)