import React from 'react';
import { StyleSheet, View } from 'react-native';
import {NewsField} from "./NewsField";

// a dummydata for news
const news = [
    {
        title: 'A meeting with our rector',
        text: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown',
        likes: '200',
        comments: '40'
    },
    {
        title: 'A meeting with our rector',
        text: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown',
        likes: '200',
        comments: '40'
    }
];

export const NewsContainer = () => {
    return (
        <View style={styles.container}>
            <View style={styles.column}>
                {news.map((news) => (
                    <NewsField
                        fontSize={{ fontSize: 20 }}
                        heading={news.title}
                        text={news.text}
                        likes={news.likes}
                        comments={news.comments}
                        style={styles.news}
                    />
                ))}
            </View>
        </View>
    );
};
const styles = StyleSheet.create({
    container: {
        marginHorizontal: 20
    },
    news: {
        alignSelf: 'center',
        width: '100%',
        height: 200
    },
    column: {
        justifyContent: 'space-around',
        alignItems: 'center',
        flexDirection: 'column'
    }
});
