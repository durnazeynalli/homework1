import React from 'react';
import {StyleSheet, View, TouchableOpacity} from 'react-native';
import { COLORS } from '../styles/colors';
import { CustomText } from './CustomText';

export const CreateBtn = () => {
    return (
        <TouchableOpacity style={styles.container}>
            <CustomText style={styles.text}>create...</CustomText>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    container: {
    borderWidth:1,
        backgroundColor: COLORS.componentsLight,
        borderRadius:4,
        paddingHorizontal: 14,
        paddingVertical: 10,
        marginHorizontal: 20,
        marginBottom: 70,
},
});
