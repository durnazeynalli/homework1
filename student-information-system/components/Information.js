import React from 'react'
import { View,StyleSheet } from 'react-native'
import { COLORS } from '../styles/colors'
import { CustomText } from './CustomText'

export const Information=()=>{
   return(
      <View style={styles.information}>
               <CustomText style={styles.informationText}>Course teacher</CustomText>
               <CustomText
                  weight="semi"
                  style={{ ...styles.informationText, color: COLORS.acsentColor, fontSize: 18 }}
               >
                  Ph.D Rakib Afandiyev
               </CustomText>
               <CustomText style={styles.informationText}>Hour: 30</CustomText>
               <CustomText style={{ ...styles.informationText, fontSize: 18, marginBottom:10}}>
                  Attendance : 17%
               </CustomText>
               {/*Dummy data for information*/}
			</View>
   )
}

const styles = StyleSheet.create({
   information: {
		marginHorizontal: 15,
		marginTop: 17
	},
	informationText: {
		color: COLORS.backgroundDark,
		marginBottom: 5,
		fontSize: 12
   },
})