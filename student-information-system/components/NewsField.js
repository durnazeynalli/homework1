import React from 'react';
import {StyleSheet, View, TouchableOpacity, Image} from 'react-native';
import { ICONS_LIGHT } from '../styles/iconsLight';
import { COLORS } from '../styles/colors';
import { CustomText } from './CustomText';
import { Seperator } from './Seperator';

export const NewsField = ({ heading, text, likes, comments, style , fontSize}) => {
    return (
        <TouchableOpacity style={{...styles.container,...style}}>
            <CustomText weight="semi" style={{...styles.heading, ...fontSize}}>{heading}</CustomText>
            <Seperator color={COLORS.backgroundDark} distance={9}/>
            <CustomText style={styles.text}>{text}</CustomText>
            <View style={styles.row}>
                <Image source={ICONS_LIGHT.commentLight} style={styles.commentIcon} />
                <CustomText style={styles.likes}>{likes}</CustomText>
                <Image source={ICONS_LIGHT.commentLight} style={styles.commentIcon} />
                <CustomText style={styles.comments}>{comments}</CustomText>
            </View>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    container:{
        borderWidth:1,
        backgroundColor: COLORS.componentsLight,
        borderRadius:4,
        paddingHorizontal:14,
        marginTop: 25,
    },
    heading:{
        color: COLORS.backgroundDark,
        marginLeft: 20,
        marginVertical: 10,
    },
    row: {
      flexDirection: "row",
        bottom: -50,
    },
    text: {
        fontSize: 14,
    },
    likes: {
        marginRight: 15,
    },
    comments: {
        marginRight: 15,
    },
    heartIcon: {
        marginBottom: 5,
    },
    commentIcon: {
        marginRight: 5,
    }
});
