import React from 'react';
import { StyleSheet, View, Image, TouchableOpacity } from 'react-native';
import { CustomText } from './CustomText';
import { COLORS } from '../styles/colors';
import { ICONS_LIGHT } from '../styles/iconsLight';
import { useNavigation } from '@react-navigation/native';

export const Header = ({ title, iconRight = {}, params}) => {
	// pass icon in the right and title as a props for each screen
	// const {navigation}=params
	return (
		<View style={styles.container}>
			<TouchableOpacity onPress={()=>navigation.toggleDrawer()}>
				<Image source={ICONS_LIGHT.drawerLight} style={styles.headerIcon}/>
			</TouchableOpacity>
			<CustomText style={styles.heading}>{title}</CustomText>
			<Image source={iconRight} style={styles.headerIcon} />
			<View style={styles.indicator} />
		</View>
	);
};
const styles = StyleSheet.create({
	container: {
		height: 60,
		backgroundColor: COLORS.backgroundLight,
		alignItems: 'center',
		justifyContent:'space-around',
		flexDirection: 'row',
		shadowColor: '#000',
		shadowOffset: {
			width: 0,
			height: 1
		},
		shadowOpacity: 0.22,
		shadowRadius: 2.22,

		elevation: 3
	},
	heading: {
		fontSize: 24,
		color: COLORS.backgroundDark,
		alignSelf: 'center',
	},
	headerIcon: {
		width: 27,
		height: 27
	},
	indicator:{
		position:'absolute',
		bottom:0,
		height:4,
		left:160,
		backgroundColor:COLORS.componentsDark,
		width: 50,
	}
});
