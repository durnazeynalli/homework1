import React from 'react';
import { StyleSheet, View } from 'react-native';
import { COLORS } from '../styles/colors';
import { CustomText } from './CustomText';


export const ProgressBar = ({ color, height, progress }) => {
	return (
		<>
			<View style={[styles.container,{height:height}]}>
				<View style={{...styles.progress, width:`${progress}%`, backgroundColor:color}} />
				{/* width will be changed depending on the progress persantage  it is a plache holder for now*/}
				<CustomText style={styles.progresstext}>20</CustomText>
			</View>
		</>
	);
};
const styles = StyleSheet.create({
	container:{
		width:'93%',
		flexDirection: 'row',
		backgroundColor:COLORS.componentsLight,
		borderRadius:4,
		alignSelf:'center',
		// overflow:'hidden',
		marginBottom:20,
	},
	progress:{
		borderRadius: 4,
	},
	progresstext:{
		position:'absolute',
		right:10,
		alignSelf:'center',
		color:COLORS.acsentColor,
		fontSize:18,
	}
});
