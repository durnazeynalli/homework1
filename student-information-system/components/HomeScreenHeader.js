import React, { useState } from 'react';
import { StyleSheet, View, TouchableOpacity } from 'react-native';
import { COLORS } from '../styles/colors';
import {CustomText} from "./CustomText";

const btns = [
    {
        name: 'News',
    },
    {
        name: 'MD1-front-end',
    },
    {
        name: 'MD2-front-end',
    }
];

export const HomeScreenHeader = ({style}) => {
    const [ indicator, setIndicator ] = useState('News');//indicator will be a props for indicate which page we r in in future
    return (
        <View style={[styles.container,{...style}]}>
            {btns.map((btn) => {
                return (

                    <TouchableOpacity
                        style={[ styles.btn, { height: btn.name === indicator ? 35 : 30 } ]}
                        onPress={() => setIndicator(btn.name)}
                    >
                        <CustomText style={styles.name}>{btn.name}</CustomText>
                        {btn.name === indicator && <View style={styles.indicator} />}
                    </TouchableOpacity>
                );
            })}
        </View>
    );
};
const styles = StyleSheet.create({
    container: {
        height: 60,
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: COLORS.backgroundLight,

        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 8
        },
        shadowOpacity: 0.46,
        shadowRadius: 11.14,
        elevation: 17
    },
    btn: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    name: {
        fontSize: 17,
    },
    indicator: {
        position: 'absolute',
        bottom: -13,
        marginTop: 10,
        backgroundColor: COLORS.componentsDark,
        width: 60,
        height: 4
    }
});
