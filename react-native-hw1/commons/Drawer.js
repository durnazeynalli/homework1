import React from "react";
import {
    StyleSheet,
    View,
    TouchableOpacity,
    Image,
} from "react-native";

import avatarImg from "../assets/avatar.png";
import { CustomText } from "../components";

export const Drawer = ({ navigation, auth: { username } }) => {
    return (
        <View style={styles.container}>

            <View style={styles.row}>
                <Image resizeMode="cover" style={styles.userImg} source={avatarImg} />
                <CustomText weight="semi" style={styles.username}>
                    {username}
                </CustomText>
            </View>

            <View style={styles.bg}>
                <TouchableOpacity style={styles.drawerBttn} onPress={() => navigation.navigate('Create')} >
                    <CustomText weight="semi" style={styles.drawerBtnLabel}>
                        add new list
                    </CustomText>
                </TouchableOpacity>

                <TouchableOpacity style={styles.drawerBtn} onPress={() => navigation.navigate('OneTime')}>
                    <CustomText weight="semi" style={styles.drawerBtnLabel}>
                        one time list
                    </CustomText>
                </TouchableOpacity>

                <TouchableOpacity style={styles.drawerBtn} onPress={() => navigation.navigate('Regular')}>
                    <CustomText weight="semi" style={styles.drawerBtnLabel}>
                        regular lists
                    </CustomText>
                </TouchableOpacity>

                <TouchableOpacity style={styles.drawerBtn} onPress={() => navigation.navigate('Settings')}>
                    <CustomText weight="semi" style={styles.drawerBtnLabel}>
                        user settings
                    </CustomText>
                </TouchableOpacity>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    closeBtn: {
        padding: 10,
        position: "absolute",
        top: 5,
        right: 5,
        zIndex: 2,
    },
    closeBtnImg: {
        width: 13,
        height: 13,
    },
    userInfo: {
        marginTop: 40,
        alignItems: "center",
    },
    userImg: {
        width: 70,
        height: 70,
        borderRadius: 35,
        overflow: "hidden",
    },
    username: {
        fontSize: 20,
        marginTop: 6,
        marginLeft: 5,
        color: "#000000",
    },
    row: {
        flexDirection: "row",
        flex: 0.2,
        justifyContent: "center",
        alignItems: "center",
    },
    iconBtn: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        paddingVertical: 13,
    },
    iconImg: {
        height: 18,
        width: 20,
    },
    bg: {
        backgroundColor: "#FF7676",
        flex: 1,
        borderTopStartRadius: 20,
        borderTopEndRadius: 20,
        paddingVertical: 10,
    },
    drawerBttn: {
        paddingVertical: 7,
        paddingHorizontal: 25,
        marginVertical: 20,
        marginHorizontal: 15,
        borderRadius: 20,
        overflow: "hidden",
        backgroundColor: "white",
    },
    drawerBtn: {
        paddingVertical: 7,
        paddingHorizontal: 25,
        marginVertical: 5,
        marginHorizontal: 15,
        borderRadius: 20,
        overflow: "hidden",
        backgroundColor: "white",
    },
    drawerBtnLabel: {
        fontSize: 14,
        color: "#FF7676",
        textAlign: "center",
        textTransform: "uppercase",
    },
});
