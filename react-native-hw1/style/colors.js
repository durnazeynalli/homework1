const COLORS = {
    main: "#464F77",
    secondary: "#868FB9",
    light: "#CAD0F0",
    btn: "#EE7676",
    add1: "#FFDDD7",
    add2: "#FBA454",
};

export default COLORS;
