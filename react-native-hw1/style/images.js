import plusWhite from "../assets/plus_white.png";
import plusGrey from "../assets/plus_grey.png";
import arrowBack from "../assets/arrowBack.png";
import dots from "../assets/dots.png";
import menu from "../assets/bars.png";
import edit from "../assets/edit.png";
import save from "../assets/interface.png";
import close from "../assets/close.png"

const images = {
  plusWhite,
  plusGrey,
  arrowBack,
  dots,
  menu,
  edit,
  save,
  close
};

export default images;
