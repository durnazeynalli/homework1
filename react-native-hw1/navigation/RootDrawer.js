import React, { useContext } from "react";
import { NavigationContainer } from "@react-navigation/native";
import {
    createDrawerNavigator,
    DrawerItem,
    DrawerContentScrollView,
} from "@react-navigation/drawer";

import { AuthStack } from "./AuthStack";
import { HomeStack } from "./HomeStack";
import { AuthCTX } from "../context/auth";
import { Drawer } from "../commons";

const { Navigator, Screen } = createDrawerNavigator();

export const RootDrawer = () => {
    const { auth } = useContext(AuthCTX);
    return (
            <Navigator
                drawerStyle={{
                    width: 222,
                }}
                drawerContent={(props) => (
                    <Drawer {...props} auth={auth} />
                )}
            >
                {auth.status ? (
                    <Screen name="Home" component={HomeStack} />
                ) : (
                    <Screen
                        name="Auth"
                        component={AuthStack}
                        options={{
                            swipeEnabled: false,
                        }}
                    />
                )}
            </Navigator>
    );
};
