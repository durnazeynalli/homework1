import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import {
  HomeScreen,
  CreateScreen,
  ListScreen,
  ProjectScreen, RegularLists, OneTimeList, InsideCard, EditCard, UserSettings, HomepageScreen,
} from "../screens";
import {RootDrawer} from "./RootDrawer";

const { Navigator, Screen } = createStackNavigator();

export const RootNav = () => (
  <NavigationContainer>
    <Navigator headerMode="none">
      <Screen name="OneTime" component={HomepageScreen} />
      <Screen name="Create" component={CreateScreen} />
      <Screen name="Drawer" component={RootDrawer}/>
      <Screen name="Regular" component={RegularLists} />
      <Screen name="InsideCard" component={InsideCard} />
      <Screen name="EditCard" component={EditCard} />
      <Screen name="Settings" component={UserSettings} />

    </Navigator>
  </NavigationContainer>
);
