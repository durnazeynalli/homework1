import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import { HomepageScreen, UserSettings} from "../screens";

const { Navigator, Screen } = createStackNavigator();

export const HomeStack = () => (
  <Navigator headerMode="none">
    <Screen name="List" component={HomepageScreen} />

  </Navigator>
);
