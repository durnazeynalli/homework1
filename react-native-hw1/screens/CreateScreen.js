import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
} from "react-native";

import { connect } from "react-redux";
import { addSection, getSections, addProject } from "../redux/data";
import COLORS from "../style/colors";
import {CustomBtn} from "../components";

const mapStateToProps = (state) => ({
  sections: getSections(state),
});

export const CreateScreen = connect(mapStateToProps, {
  addSection,
  addProject,
})(({ addSection, addProject, navigation, sections }) => {
  const [type, setType] = useState("project");
  const [fields, setFields] = useState({
    sectionID: "",
    name: "",
    imgUri: "",
  });

  const fieldChangeHandler = (name, value) => {
    setFields((fields) => ({
      ...fields,
      [name]: value,
    }));
  };

  const isProject = type === "project";

  const handleAdd = () => {
    if (type === "section" ) {
      addSection(fields.name);
      navigation.navigate("Home");
    } else if (type === "project" && fields.name.trim() !== "") {
      addSection(fields.name);
      navigation.navigate("Regular");
    }
  };

  return (
    <View style={styles.container}>
      <Text style={styles.heading}>New List</Text>

      <View style={styles.paddingHorizontal}>
        <Text>list name</Text>

        <TextInput
          onChangeText={(val) => fieldChangeHandler("name", val)}
          placeholder="name"
          style={styles.field}
        />

        <View style={styles.row}>
          <TouchableOpacity
              style={[styles.drawerBtn, styles.typeField]}
              onPress={() => setType("section")}
          >
            <Text
                style={[
                  styles.text,
                  { fontWeight: type === "section" ? "bold" : "400" },
                ]}
            >
              One Time
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
              style={[styles.drawerBtn, styles.typeField]}
              onPress={() => setType("project")}
          >
            <Text
                style={[
                  styles.text,
                  { fontWeight: type === "project" ? "bold" : "400" },
                ]}
            >
              Regular
            </Text>
          </TouchableOpacity>
        </View>

        <CustomBtn
            onPress={ handleAdd }
            style={styles.btn}
            title="create list"
        />
      </View>
    </View>
  );
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.btn,
  },
  heading: {
    textAlign: "center",
    color: "white",
    marginVertical: 30,
    fontSize: 20,
  },
  row: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 10,
  },
  paddingHorizontal: {
    backgroundColor: "white",
    flex: 1,
    borderTopStartRadius: 20,
    borderTopEndRadius: 20,
    alignItems: "center",
    paddingVertical: 30,
  },
  field: {
    backgroundColor: "#EEEEEE",
    width: "85%",
    borderRadius: 20,
    marginVertical: 10,
    paddingVertical: 10,
    textAlign: "center",
  },
  typeField: {
    width: "40%",
  },
  drawerBtn: {
    backgroundColor: "#EEEEEE",
    borderRadius: 20,
    paddingVertical: 10,
    marginHorizontal: 11,
    marginVertical: 10,
  },
  text: {
    textAlign: "center",
  },
  btn: {
    width: "85%",
    marginVertical: 15,
  },
  sections: {
    paddingVertical: 20,
  },
  sectionTitle: {
    textAlign: "center",
  },
});
