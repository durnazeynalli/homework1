import React, {useState} from "react";
import {StyleSheet, View, Text, TouchableOpacity, Image, FlatList} from "react-native";
import images from "../style/images";
import {CustomBtn, SectionCard, ProjectCard} from "../components";
import {Nav} from "../commons/Nav";
import { useNavigation } from "@react-navigation/native";
import {connect} from "react-redux";
import {getSections, toggleTodoState} from "../redux/data";

const mapStateToProps = (state) => ({
    sections: getSections(state),
});

export const InsideCard = connect(mapStateToProps, {    toggleTodoState,
})((props) => {
    const { sections, sectionID, projectID } = props;
    const navigation = useNavigation();

    const {  todos = [] } =
    sections
        .find((section) => section.id === sectionID)
        ?.sections.find((section) => section.id === sectionID) || {};

    const sortedTodos = todos.sort((a, b) => {
        if (a.done === b.done) return 0;
        if (a.done) return 1;
        if (b.done) return -1;
    });

    return (
        <View style={styles.container}>
            <View style={styles.row}>
                <Nav back={true} create={false} />
                <Text style={styles.head}> inside </Text>

                <TouchableOpacity onPress={() => navigation.navigate("EditCard", { sectionID, projectID })}>
                    <Image style={styles.BtnImg} source={images.edit} />
                </TouchableOpacity>
            </View>
            <View style={styles.bg}>
                <View style={styles.row}>
                    <CustomBtn
                        onPress={ () => {alert('reset')} }
                        style={styles.btn}
                        title="reset"
                    />
                    <Text style={styles.text}>0/0</Text>
                </View>

                {sections.map((section) => (
                    <FlatList
                        key={section.id}
                        contentContainerStyle={styles.projectsList}
                         data={section.projects}
                        renderItem={({ item }) => (
                            <ProjectCard
                                title={item.text}
                                count={item.count}
                                image={item.imgUri}
                                sectionID={section.id}
                                projectID={item.id}
                            />
                        )}
                    />
                ))}
            </View>
        </View>
    );
});

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#EE7676",

    },
    head: {
        textAlign: "center",
        color: "white",
        marginVertical: 30,
        fontSize: 22,
    },
    bg: {
        backgroundColor: "white",
        flex: 1,
        borderTopStartRadius: 20,
        borderTopEndRadius: 20,
        paddingVertical: 20,
        paddingHorizontal: 20,
    },
    inputTxt: {
        backgroundColor: "#EEEEEE",
        width: "85%",
        borderRadius: 20,
        marginVertical: 10,
        paddingVertical: 10,
        textAlign: "center",
    },
    row: {
        flexDirection: "row",
        justifyContent: "space-between"
    },
    drawerBtn: {
        backgroundColor: "#EEEEEE",
        width: "40%",
        borderRadius: 20,
        paddingVertical: 10,
        marginHorizontal: 11,
        marginVertical: 10,
    },
    drawerBtnLabel: {
        textAlign: "center",
    },
    btn: {
        width: "30%",
    },
    text: {
      fontSize: 20,
        marginTop: 10,
        color: "#303234",
    },
    BtnImg: {
        width: 25,
        height: 25,
        marginVertical: 30,
        marginRight: 15,
    },

});
