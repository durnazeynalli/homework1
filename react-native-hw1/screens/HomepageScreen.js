import React from "react";
import {StyleSheet, View, Text, TouchableOpacity, Image} from "react-native";
import images from "../style/images";
import {SectionCard} from "../components";
import {connect} from "react-redux";
import {getSections} from "../redux/data";

const mapStateToProps = (state) => ({
    sections: getSections(state),
});


export const HomepageScreen = connect(mapStateToProps)((props) => {
    const { navigation, sections, sectionID, projectID } = props;

  return (
    <View style={styles.container}>
        <View style={styles.row}>
            <Text style={styles.head}>One Time List</Text>
            <TouchableOpacity onPress={() => navigation.navigate("Drawer")}>
                <Image style={styles.BtnImg} source={images.menu} />
            </TouchableOpacity>
        </View>
        <View style={styles.bg}>
            <View >
                {sections.map((section) => (
                    <TouchableOpacity onPress={() => navigation.navigate('InsideCard', { sectionID, projectID })}>
                        <SectionCard key={section.id} section={section} />
                    </TouchableOpacity>
                ))}
            </View>
        </View>
    </View>
  );
});

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#EE7676",

    },
    head: {
        textAlign: "center",
        color: "white",
        marginVertical: 30,
        marginLeft: 128,
        fontSize: 20,
    },
    bg: {
        backgroundColor: "white",
        flex: 1,
        borderTopStartRadius: 20,
        borderTopEndRadius: 20,
        paddingVertical: 30,
        paddingHorizontal: 20,
    },
    row: {
        flexDirection: "row",
    },
    btn: {
        width: "85%",
        marginVertical: 15,
    },
    BtnImg: {
        width: 25,
        height: 25,
        marginVertical: 30,
        marginLeft: 90,
    },

});
