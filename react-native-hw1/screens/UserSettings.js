import React, { useState } from "react";
import {StyleSheet, View, Text, TextInput, TouchableOpacity, Button} from "react-native";
import {CustomBtn, CustomText} from "../components";

export const UserSettings = () => {

    const [fields, setFields] = useState({
        sectionID: "",
        name: "",
        imgUri: "",
    });

    const fieldChangeHandler = (name, value) => {
        setFields((fields) => ({
            ...fields,
            [name]: value,
        }));
    };

    return (
        <View style={styles.container}>
            <Text style={styles.head}>User Settings</Text>
            <View style={styles.bg}>
                <Text>username</Text>
                <TextInput style={styles.inputTxt}/>

                <Text>avatar url</Text>
                <TextInput
                    style={styles.inputTxt}
                    onChangeText={(val) => fieldChangeHandler("imgUri", val)}
                />

                <CustomBtn
                    onPress={() => {alert('created')}}
                    style={styles.btn}
                    title="save changes"
                />

            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#EE7676",
    },
    head: {
        textAlign: "center",
        color: "white",
        marginVertical: 30,
        fontSize: 20,
    },
    bg: {
        backgroundColor: "white",
        flex: 1,
        borderTopStartRadius: 20,
        borderTopEndRadius: 20,
        alignItems: "center",
        paddingVertical: 30,
    },
    inputTxt: {
        backgroundColor: "#EEEEEE",
        width: "85%",
        borderRadius: 20,
        marginVertical: 10,
        paddingVertical: 10,
        textAlign: "center",
    },
    btn: {
        width: "85%",
        marginVertical: 15,
    }

});
