import React, { useState } from "react";
import {
    StyleSheet,
    Text,
    View,
    FlatList,
    TouchableOpacity,
    Alert, Image,
} from "react-native";
import { Nav } from "../commons/Nav";
import { connect } from "react-redux";
import {
    getSections,
    toggleTodoState,
    addTodo,
    deleteTodo,
    updateTodo,
} from "../redux/data";
import { ProjectCard, TodoForm } from "../components";
import images from "../style/images";

const mapStateToProps = (state) => ({
    sections: getSections(state),
});

export const EditCard = connect(mapStateToProps, {
    toggleTodoState,
    addTodo,
    deleteTodo,
    updateTodo,
})(({ route, sections, toggleTodoState, addTodo, deleteTodo, updateTodo }) => {
    const {
        params: { sectionID, projectID },
    } = route;

    const [isEditMode, setIsEditMode] = useState(false);
    const [editTodoID, setEditTodoId] = useState(null);

    const { name = "404", imgUri, todos = [] } =
    sections
        .find((section) => section.id === sectionID)
        ?.projects.find((project) => project.id === projectID) || {};

    const sortedTodos = todos.sort((a, b) => {
        if (a.done === b.done) return 0;
        if (a.done) return 1;
        if (b.done) return -1;
    });

    const submitTodoForm = (todoText) => {
        const args = {
            sectionID,
            projectID,
            todoText,
        };
        if (isEditMode) {
            updateTodo({ ...args, todoID: editTodoID });
            setIsEditMode(false);
            setEditTodoId(null);
        } else {
            addTodo(args);
        }
    };

    const startEditTodo = (id) => {
        setIsEditMode(true);
        setEditTodoId(id);
    };

    const handleTodoLongPress = (todoID) =>  {deleteTodo({ sectionID, projectID, todoID })};

    return (
        <View style={styles.container}>
            <View style={styles.row}>
                <Nav back={true} create={false} />
                <Text style={styles.head}> inside </Text>

                <TouchableOpacity onPress={() => navigation.navigate("EditCard", { sectionID, projectID })}>
                    <Image style={styles.BttnImg} source={images.edit} />
                </TouchableOpacity>
            </View>
            <View style={styles.bg}>

                {isEditMode ? <TodoForm btnName="update" onSubmit={submitTodoForm} /> :
                    <TodoForm btnName="add to list" onSubmit={submitTodoForm} />}

                <FlatList
                    contentContainerStyle={styles.list}
                    data={sortedTodos}
                    renderItem={({ item }) => (
                        <View style={styles.todoItemWrapper}>


                            <TouchableOpacity
                                onPress={() =>
                                    toggleTodoState({
                                        sectionID,
                                        projectID,
                                        todoID: item.id,
                                    })
                                }
                            >
                                <View
                                    style={[styles.todoItem, { opacity: item.done ? 0.5 : 1 }]}
                                >
                                    <Text style={styles.txt}>{item.text}</Text>
                                </View>
                            </TouchableOpacity>

                            <TouchableOpacity style={styles.icons} onPress={() => startEditTodo(item.id)}>
                                <Image style={styles.BtnImg} source={images.edit} />
                            </TouchableOpacity>

                            <TouchableOpacity style={styles.icon} onPress={ handleTodoLongPress }>
                                <Image style={styles.BtnImg} source={images.close} />
                            </TouchableOpacity>
                        </View>
                    )}
                />
            </View>
        </View>
    );
});

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#EE7676",

    },
    head: {
        textAlign: "center",
        color: "white",
        marginVertical: 30,
        fontSize: 22,
    },
    row: {
        flexDirection: "row",
        justifyContent: "space-between"
    },
    bg: {
        backgroundColor: "white",
        flex: 1,
        borderTopStartRadius: 20,
        borderTopEndRadius: 20,
        paddingVertical: 20,
        paddingHorizontal: 20,
    },
    BttnImg: {
        width: 25,
        height: 25,
        marginVertical: 30,
        marginRight: 15,
    },
    list: {
        marginTop: 10,
        borderRadius: 15,
        paddingHorizontal: 25,
        paddingVertical: 20,
    },
    todoItemWrapper: {
        marginBottom: 8,
        borderRadius: 8,
        flexDirection: "row",
    },
    todoItem: {
        backgroundColor: "white",
        borderRadius: 20,
        paddingVertical: 10,
        paddingHorizontal: 14,
        borderWidth: 1,
        borderColor: "#FFD976",
        width: 290,
        marginBottom: 10,
    },
    txt: {
        textAlign: "center",
    },
    BtnImg: {
        width: 25,
        height: 25,
    },
    icon: {
        backgroundColor: "#EE7676",
        borderColor: "#EE7676",
        borderWidth: 1,
        borderRadius: 100,
        margin: 5,
        marginLeft: -30,
        paddingHorizontal: 8,
        paddingVertical: 8,
    },
    icons: {
        backgroundColor: "#FFD976",
        borderColor: "#FFD976",
        borderWidth: 1,
        borderRadius: 100,
        margin: 5,
        marginLeft: -300,
        marginRight: 250,
        paddingHorizontal: 8,
        paddingVertical: 8,
    }
});
