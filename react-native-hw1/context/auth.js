import React, { createContext, useState } from "react";
import { AsyncStorage, Alert } from "react-native";

export const AuthCTX = createContext();

export const AuthCTXProvider = ({ children }) => {
  const [auth, setAuth] = useState({
    status: true,
    username: "durnazeynalli",
  });

  const signUpHandler = async (username, password) => {
    try {
      const isUserExist = await AsyncStorage.getItem(username);
      if (isUserExist) {
        return {
          error: true,
          errorType: "exist",
          errorTitle: "Sorry, user with this username already exist",
          errorDesc: "Try to login",
        };
      }

      const create = await AsyncStorage.setItem(username, password);
      if (create) {
        return {
          error: true,
          errorType: "write_err",
          errorTitle: "Sorry, something goes wrong",
          errorDesc: "try later",
        };
      }

      setAuth((auth) => ({
        ...auth,
        status: true,
        username,
      }));

      return {
        error: false,
      };
    } catch (e) {
      console.log(e);
    }
  };

  const loginHandler = async (username, password) => {
    try {
      const storedPassword = await AsyncStorage.getItem(username);

      if (!storedPassword) {
        return {
          error: true,
          errorType: "non_exist",
          errorTitle: "Sorry, user non exist",
          errorDesc: "Try to register",
        };
      }

      if (storedPassword !== password) {
        Alert.alert("Sorry, password didnt match");
        return {
          error: true,
          errorType: "pass_wrong",
          errorTitle: "Sorry, password didnt match",
          errorDesc: "",
        };
      }

      setAuth((auth) => ({
        ...auth,
        status: true,
        username,
      }));

      return {
        error: false,
      };
    } catch (e) {
      console.log(e);
    }
  };

  const logOutHandler = () => {
    setAuth((auth) => ({
      ...auth,
      status: false,
      username: "",
    }));
  };

  return (
    <AuthCTX.Provider
      value={{ auth, signUpHandler, logOutHandler, loginHandler }}
    >
      {children}
    </AuthCTX.Provider>
  );
};
