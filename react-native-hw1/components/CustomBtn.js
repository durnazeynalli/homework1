import React from "react";
import {
  StyleSheet,
  View,
  TouchableOpacity,
  TouchableNativeFeedback,
  Platform,
} from "react-native";

import { CustomText } from "./CustomText";
import COLORS from "../style/colors";

export const CustomBtn = ({ title, onPress, style, ...rest }) => {
  const Touchable =
    Platform.OS === "android" ? TouchableNativeFeedback : TouchableOpacity;
  return (
    <View style={[styles.container, style]}>
      <Touchable onPress={onPress} {...rest}>
        <View style={styles.btn}>
          <CustomText weight="semi" style={styles.title} numberOfLines={1}>
            {title}
          </CustomText>
        </View>
      </Touchable>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: "100%",
    borderRadius: 30,
    overflow: "hidden",
  },
  btn: {
    backgroundColor: COLORS.btn,
    borderRadius: 30,
    paddingVertical: 10,
    paddingHorizontal: 30,
  },
  title: {
    color: "white",
    fontSize: 14,
    textTransform: "uppercase",
    textAlign: "center",
  },
});
