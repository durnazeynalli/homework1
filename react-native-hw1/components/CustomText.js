import React from "react";
import { Text } from "react-native";

const fontFamilies = {
  regular: "OpenSansRegular",
  semi: "OpenSansSemiBold",
  bold: "OpenSansBold",
};

export const CustomText = ({ children, weight, style, ...rest }) => (
  <Text
    {...rest}
    style={[
      { fontFamily: fontFamilies[weight] || fontFamilies.regular },
      style,
    ]}
  >
    {children}
  </Text>
);
