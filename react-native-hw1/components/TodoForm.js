import React, { useState } from "react";
import {
  StyleSheet,
  View,
  TextInput,
  TouchableOpacity,
  Text,
} from "react-native";
import {CustomBtn} from "./CustomBtn";

export const TodoForm = ({ onSubmit, btnName }) => {
  const [value, setValue] = useState("");
  const [type, setType] = useState("");


  const onAddPress = () => {
    if (value.trim() !== "") {
      onSubmit(value);
      setValue("");
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.row}>
        <View>
          <Text style={styles.text}>position name</Text>
          <TextInput
              placeholder="position name"
              style={styles.inputTxt}
              value={value} onChangeText={setValue}
          />
        </View>
        <View>
          <Text style={styles.text}>count</Text>
          <Text style={styles.count}> 0 </Text>
        </View>
      </View>
      <View style={styles.row}>
        <TouchableOpacity
            style={[styles.drawerBtn, styles.typeField]}
            onPress={() => setType("pkg")}
        >
          <Text
              style={[
                styles.text,
                { fontWeight: type === "pkg" ? "bold" : "400" },
              ]}
          >
            pkg
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
            style={[styles.drawerBtn, styles.typeField]}
            onPress={() => setType("kg")}
        >
          <Text
              style={[
                styles.text,
                { fontWeight: type === "kg" ? "bold" : "400" },
              ]}
          >
            kg
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
            style={[styles.drawerBtn, styles.typeField]}
            onPress={() => setType("litre")}
        >
          <Text
              style={[
                styles.text,
                { fontWeight: type === "litre" ? "bold" : "400" },
              ]}
          >
            litre
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
            style={[styles.drawerBtn, styles.typeField]}
            onPress={() => setType("bott")}
        >
          <Text
              style={[
                styles.text,
                { fontWeight: type === "bott" ? "bold" : "400" },
              ]}
          >
            bott
          </Text>
        </TouchableOpacity>

      </View>
      <CustomBtn
          onPress={ onAddPress }
          style={styles.btn}
          title={btnName}
      />

      <View style={styles.line}/>

    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
    justifyContent: "space-between",
  },

  text: {
    textAlign: "center",
  },
  inputTxt: {
    backgroundColor: "#EEEEEE",
    width: 200,
    borderRadius: 20,
    marginVertical: 15,
    paddingVertical: 10,
    paddingHorizontal: 30,
    textAlign: "center",
  },
  count: {
    backgroundColor: "#EEEEEE",
    width: 110,
    borderRadius: 20,
    marginVertical: 15,
    paddingVertical: 10,
    textAlign: "center",
  },
  row: {
    flexDirection: "row",
    justifyContent: "space-between"
  },
  BtnImg: {
    width: 25,
    height: 25,
    marginVertical: 30,
    marginRight: 15,
  },
  typeField: {
    width: "22%",
  },
  drawerBtn: {
    backgroundColor: "#EEEEEE",
    borderRadius: 20,
    paddingVertical: 10,
    marginHorizontal: 6,
    marginVertical: 10,
  },
  btn: {
    marginVertical: 20,
  },
  line: {
    borderColor: "#EEEEEE",
    width: 600,
    borderWidth: 2,
    marginHorizontal: -20,
    marginVertical: 10,

  }

});
