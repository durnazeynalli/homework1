import React, { useState } from "react";
import { AppLoading } from "expo";

import { loadFonts } from "./style/fonts";
import { AuthCTXProvider } from "./context/auth";

import { RootNav } from "./navigation";
import { Provider } from "react-redux";
import store from "./redux";

export default function App() {
    const [loaded, setLoaded] = useState(false);

    if (!loaded) {
        return (
            <AppLoading
                startAsync={loadFonts}
                onFinish={() => setLoaded(true)}
                onError={() => console.log("Loading failed")}
            />
        );
    }

  return (
    <Provider store={store}>
        <AuthCTXProvider>
            <RootNav />
        </AuthCTXProvider>
    </Provider>
  );
}
