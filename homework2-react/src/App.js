import React from 'react';
import { BrowserRouter as Router, Switch, Route, NavLink } from 'react-router-dom';

import {
  Homepage,
} from './pages'

function App() {
  return (
      <Router>
        <header className="homepage-head">
          <div className="row space-between">
            <h3 className="store-name">Music Store</h3>
            <nav className="navigation">
              <NavLink className="nav-list" exact to="/">Home</NavLink>
              <NavLink className="nav-list" to="/">Discs</NavLink>
              <NavLink className="nav-list" to="/">Fav</NavLink>
              <NavLink className="nav-list" to="/">Card</NavLink>
              <NavLink className="nav-list" to="/">Login/Sign up</NavLink>
            </nav>
          </div>
        </header>
        <Switch>
          <Route exact path="/" component={Homepage} />
        </Switch>
      </Router>
  );
}

export default App;
