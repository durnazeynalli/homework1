import React from "react";

export const Modal = ({header, closeIcon, actions, text, close}) => {
    return (
            <div className="modal">
                <header>
                    {header}
                    {closeIcon && <p onClick={close} className="close-btn">x</p>}
                </header>
                <div className="modal-body">
                    <p className="modal-text">{text}</p>
                    {actions}
                </div>
            </div>

    )
}