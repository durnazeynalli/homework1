import React from 'react';
import { FaStar } from 'react-icons/fa';
import { Link } from 'react-router-dom';

export const ArticleListItem = ({ title, price, id, number, img, btn_fav, btn_card, color }) => {

    return (
        <Link
            className="article-list-item">
            <img className="article-img" src={img}/>
            <h3>{title}</h3>
            <div className="price-number row space-between">
                <h4>${price}</h4>
                <h4>{number}</h4>
                <h3 style={{color}}
                    onClick={btn_fav}><FaStar /></h3>
            </div>
            <div className="row space-between">
                <div className="buttons">
                    <button onClick={btn_card}>Add to cart</button>
                </div>
            </div>
        </Link>
    )
};
