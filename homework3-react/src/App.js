import React from 'react';
import { FaStar } from 'react-icons/fa';
import { FaCartArrowDown } from 'react-icons/fa';
import { FaSignInAlt } from 'react-icons/fa';
import { BrowserRouter as Router, Switch, Route, NavLink } from 'react-router-dom';

import {
    Homepage,
    Card,
    Favorite
} from './pages'

function App() {
  return (
      <Router>
        <header className="homepage-head">
          <div className="row space-between">
            <h3 className="store-name">Music Store</h3>
            <nav className="navigation">
              <NavLink className="nav-list" exact to="/">Home</NavLink>
              <NavLink className="nav-list" to="/">Discs</NavLink>
              <NavLink className="nav-list" to="/favorite"><FaStar /></NavLink>
              <NavLink className="nav-list" to="/card"><FaCartArrowDown /></NavLink>
              <NavLink className="nav-list" to="/"><FaSignInAlt /></NavLink>
            </nav>
          </div>
        </header>
        <Switch>
            <Route exact path="/" component={Homepage} />
            <Route path="/card" component={Card} />
            <Route path="/favorite" component={Favorite} />
            <Route render={() => (<h1>404</h1>)} />
        </Switch>
      </Router>
  );
}

export default App;
